import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FinanceComponent } from './finance/finance.component';
import { HrComponent } from './hr/hr.component';
import { TechnologyComponent } from './technology/technology.component';

const routes: Routes = [
  {path:'tech',component:TechnologyComponent},
  {path:'hr',component:HrComponent},
  {path:'finance',component:FinanceComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
