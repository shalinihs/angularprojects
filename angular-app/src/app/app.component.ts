import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-app';
  name="RAMA";
  employee={name:'Krishna',location:'Mysore'};
  imageUrl="assets/Images/Angular.png";
}
