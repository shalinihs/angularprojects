import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'class_Binding';

  classToApply:string='fontClass colorClass italicClass';

  classToExclude:string='fontClass colorClass';

  applyFontClass:boolean=true;
  applyItallicClass:boolean=false;
addCSSClasses(){
  let cssClasses={
    fontClass:this.applyFontClass,
    ititalicClass:this.applyItallicClass

  }
  return cssClasses;
}


}
