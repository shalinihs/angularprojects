import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'style_Binding';

  isBold:boolean=true;
  isLarge:boolean=true;

  fontSize:number=60;

  addCssStyle(){
    let cssStyles={
      'font-weight':"isBold ? 'bold' : 'normal'",
      'font-style':"isLarge?'large':'small'",
      'font-size.px':this.fontSize,
      'color':'green'

    }
    return cssStyles;
  }
}
