import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'attribute_Binding';
  pageHeader='Employee Details';
  firstName='Sita';
  lastName='Rama';
  department='IT';
  columnSpan:number=2;
}
