import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'event_Binding';
  onClick(event: any){
    console.log('Mouse click Event',event);
  }
  onKeyPressed(event:any){
    console.log('On Key Pressed',event.target.value);
 
  }
}
